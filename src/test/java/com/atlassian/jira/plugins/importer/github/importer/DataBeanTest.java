package com.atlassian.jira.plugins.importer.github.importer;

import com.atlassian.jira.plugins.importer.imports.importer.impl.DefaultJiraDataImporter;
import junit.framework.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class DataBeanTest {

    @Test
    public void testIssueLinkConvertion() {
        Map<String, String> externalIdsToIssueKeys = new HashMap<String, String>() {{
            put("1", "ABC-11");
            put("2", "ABC-12");
            put("30", "ABC-130");
            put("9999", "ABC-1999");
        }};

        String input, output;

        input = "Relates to issue #1 and also...";
        output = DefaultJiraDataImporter.rewriteStringWithIssueKeys(DataBean.ISSUE_KEY_REGEX, externalIdsToIssueKeys, input);
        Assert.assertEquals("Relates to issue ABC-11 and also...", output);

        input = "Relates to issue # 1 and also...";
        output = DefaultJiraDataImporter.rewriteStringWithIssueKeys(DataBean.ISSUE_KEY_REGEX, externalIdsToIssueKeys, input);
        Assert.assertEquals(input, output);

        input = " #9999 bot #abc but ##2.";
        output = DefaultJiraDataImporter.rewriteStringWithIssueKeys(DataBean.ISSUE_KEY_REGEX, externalIdsToIssueKeys, input);
        Assert.assertEquals(" ABC-1999 bot #abc but #ABC-12.", output);
    }

}
