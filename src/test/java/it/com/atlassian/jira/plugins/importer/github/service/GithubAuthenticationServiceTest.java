package it.com.atlassian.jira.plugins.importer.github.service;


import com.atlassian.jira.plugins.importer.github.fetch.GithubAuthenticationService;
import junit.framework.Assert;
import org.junit.Test;

public class GithubAuthenticationServiceTest {

    @Test
    public void testInvalidAuthentication() throws ClassNotFoundException {
        GithubAuthenticationService service = new GithubAuthenticationService();
        GithubAuthenticationService.AccessTokenResponse response = service.retrievePermanentAccessToken("invalidcode");
        Assert.assertNull(response.access_token);
        Assert.assertEquals("bad_verification_code", response.error);
    }

}
