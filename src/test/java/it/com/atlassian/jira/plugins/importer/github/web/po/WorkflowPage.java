package it.com.atlassian.jira.plugins.importer.github.web.po;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.plugins.importer.github.rest.workflow.WorkflowResource;
import com.atlassian.jira.plugins.importer.po.common.AbstractImporterWizardPage;
import com.atlassian.jira.plugins.importer.po.common.ImporterLogsPage;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;
import org.junit.Assert;
import org.openqa.selenium.By;

public class WorkflowPage extends AbstractImporterWizardPage {

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    public ImporterLogsPage next() {
        Assert.assertTrue(nextButton.isEnabled());
        nextButton.click();
        return pageBinder.bind(ImporterLogsPage.class);
    }

    private void selectWorkflowScheme(String schemeName) {
        String id = com.atlassian.jira.plugins.importer.github.web.WorkflowPage.SCHEME_SELECT_ID;
        SelectElement workflowSchemeSelect = elementFinder.find(By.id(id), SelectElement.class);
        workflowSchemeSelect.select(Options.value(schemeName));
    }

    public void selectDefaultWorkflowScheme() {
        selectWorkflowScheme(WorkflowResource.DEFAULT_WORKFLOW_SCHEME_KEY);
    }

    public void selectJiraWorkflowClosedStatus() {
        SelectElement jiraClosedStatusSelect = elementFinder.find(By.name("jira_closed_status"), SelectElement.class);
        jiraClosedStatusSelect.select(Options.value(String.valueOf(IssueFieldConstants.CLOSED_STATUS_ID)));
    }

}
