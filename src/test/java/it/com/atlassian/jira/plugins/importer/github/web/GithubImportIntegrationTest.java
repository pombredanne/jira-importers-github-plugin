package it.com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.plugins.importer.po.ExternalImportPage;
import com.atlassian.jira.plugins.importer.po.common.ImporterFinishedPage;
import com.atlassian.jira.plugins.importer.po.common.ImporterLogsPage;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.google.common.base.Predicate;
import it.com.atlassian.jira.plugins.importer.github.web.po.AuthenticationPage;
import it.com.atlassian.jira.plugins.importer.github.web.po.ImporterProjectsMappingsPage;
import it.com.atlassian.jira.plugins.importer.github.web.po.LabelMappingPage;
import it.com.atlassian.jira.plugins.importer.github.web.po.WorkflowPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nullable;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class GithubImportIntegrationTest {
	private JiraTestedProduct jira;
	private Backdoor backdoor;

    private static final String GITHUB_TEST_LOGIN = "PluginCharlie";
    private static final String GITHUB_TEST_PASSWORD = "shipit23";

	@Before
	public void setUp() {
        // set properties for in-IDE testing
        if( System.getProperty("jira.host") == null ) {
            System.setProperty("jira.host", "localhost");
            System.setProperty("jira.port", "2990");
            System.setProperty("jira.xml.data.location", "src/test");
        }

		backdoor = new Backdoor(new TestKitLocalEnvironmentData());
		backdoor.restoreBlankInstance();
        backdoor.attachments().enable();
		jira = new JiraTestedProduct(null, new EnvironmentBasedProductInstance());
	}

    @Test
    public void testAttachedToConfigPage() {
        assertThat(jira.gotoLoginPage().loginAsSysAdmin(ExternalImportPage.class).getImportersOrder(), hasItem("GitHub"));
    }

	@Test
	public void testWizard() {
//        try {

        final WebDriver driver = jira.getTester().getDriver();
        final WebDriverWait wait = new WebDriverWait(driver, 10);

        AuthenticationPage setupPage = jira.gotoLoginPage().loginAsSysAdmin(AuthenticationPage.class);
        setupPage.next();

        // wait for GitHub login page
        By byLogin_field = By.id("login_field");
        wait.until(ExpectedConditions.presenceOfElementLocated(byLogin_field));

        // fill out GitHub login page
        driver.findElement(byLogin_field).sendKeys(GITHUB_TEST_LOGIN);
        driver.findElement(By.id("password")).sendKeys(GITHUB_TEST_PASSWORD);
        driver.findElement(By.name("commit")).click();

        // wait for GitHub authorize or our security question page
        final By byAuthorize = By.name("authorize");
        final By byContinueTrigger = By.id("continueTrigger");
        wait.until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(@Nullable WebDriver input) {
                return !driver.findElements(byAuthorize).isEmpty() || !driver.findElements(byContinueTrigger).isEmpty();
            }
        });

        // if necessary authorize on GitHub
        if( !driver.findElements(byAuthorize).isEmpty() ) {
            driver.findElement(byAuthorize).click();
        }

        // confirm our security question
        wait.until(ExpectedConditions.presenceOfElementLocated(byContinueTrigger));
        driver.findElement(byContinueTrigger).click();

        // project mapping
		ImporterProjectsMappingsPage projectsPage = setupPage.getNext();
        projectsPage.doWait();
        projectsPage.setImportAllProjects(false);
        projectsPage.setProjectImported("PluginCharlie/Dragonslayer", true);
        projectsPage.createProject("PluginCharlie/Dragonslayer", "Dragonslayer JIRA Project", "DRA");

        // labels
        LabelMappingPage labelMappingPage = projectsPage.next2();
        labelMappingPage.selectLabelIssueTypeMapping("enhancement", IssueFieldConstants.IMPROVEMENT_TYPE_ID);
        labelMappingPage.selectLabelIssueResolutionMapping("wontfix", IssueFieldConstants.WONTFIX_RESOLUTION_ID);

        // workflow
        WorkflowPage workflowPage = labelMappingPage.next();
        workflowPage.selectDefaultWorkflowScheme();
        workflowPage.selectJiraWorkflowClosedStatus();

        // perform import
        ImporterLogsPage importerLogsPage = workflowPage.next();
        ImporterFinishedPage importerFinishedPage = importerLogsPage.waitUntilFinished();
		assertTrue(importerFinishedPage.isSuccess());

        // validate import
		SearchResult search = backdoor.search().getSearch(new SearchRequest().jql(""));
		assertEquals((Integer) 5, search.total);

        List<Issue> issues;
        Issue issue;

        // verify issue #1
        issues = backdoor.search().getSearch(new SearchRequest().jql("project = DRA AND 'External issue ID' ~ '1'")).issues;
        assertEquals(1, issues.size());
        issue = issues.get(0);
        assertEquals("Build a new cave", issue.fields.summary);
        assertEquals(String.valueOf(IssueFieldConstants.CLOSED_STATUS_ID), issue.fields.status.id());
        assertEquals(String.valueOf(IssueFieldConstants.IMPROVEMENT_TYPE_ID), issue.fields.issuetype.id);
        assertEquals(String.valueOf(IssueFieldConstants.WONTFIX_RESOLUTION_ID), issue.fields.resolution.id);
        assertNotNull(issue.fields.assignee);
        assertEquals(1, issue.fields.fixVersions.size());
        assertEquals("Beta 1", issue.fields.fixVersions.get(0).name);

        // verify issue #2
        issues = backdoor.search().getSearch(new SearchRequest().jql("project = DRA AND 'External issue ID' ~ '2'")).issues;
        assertEquals(1, issues.size());
        issue = issues.get(0);
        assertEquals(String.valueOf(IssueFieldConstants.OPEN_STATUS_ID), issue.fields.status.id());
        assertEquals(1, issue.fields.fixVersions.size());
        assertEquals("Version 1", issue.fields.fixVersions.get(0).name);

        // verify issue #5
        issues = backdoor.search().getSearch(new SearchRequest().jql("project = DRA AND 'External issue ID' ~ '5'").fields("attachment")).issues;
        assertEquals(1, issues.size());
        issue = issues.get(0);
        assertEquals(1, issue.fields.attachment.size());
        assertEquals("landscape_7.jpg", issue.fields.attachment.get(0).filename);

//        } finally { JOptionPane.showMessageDialog(null, "Click to close"); }
    }

}
