package it.com.atlassian.jira.plugins.importer.github.web.po;

import com.atlassian.jira.plugins.importer.po.common.AbstractImporterWizardPage;
import com.atlassian.jira.plugins.importer.po.common.ImporterLogsPage;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;
import org.junit.Assert;
import org.openqa.selenium.By;

public class LabelMappingPage extends AbstractImporterWizardPage {

    @Override
    public String getUrl() {
        return "/secure/admin/views/LabelMappingPage!default.jspa";
    }

    public WorkflowPage next() {
        Assert.assertTrue(nextButton.isEnabled());
        nextButton.click();
        return pageBinder.bind(WorkflowPage.class);
    }

    public void selectLabelIssueTypeMapping(String label, int issueTypeId) {
        SelectElement typeMappingSelect = elementFinder.find(By.id("typeMapping_" + label), SelectElement.class);
        typeMappingSelect.select(Options.value(String.valueOf(issueTypeId)));
    }

    public void selectLabelIssueResolutionMapping(String label, int issueResolutionId) {
        SelectElement issueResolutionSelect = elementFinder.find(By.id("resolutionMapping_" + label), SelectElement.class);
        issueResolutionSelect.select(Options.value(String.valueOf(issueResolutionId)));
    }

}
