(function($) {
    "use strict";

    AJS.GithubImporter = AJS.GithubImporter != null ? AJS.GithubImporter : {};

    AJS.GithubImporter.Workflow = {
        statusMapping: null,

        schemeSelect: function(selectElement){
            var that = this;
            selectElement.find("option:selected").each(function(){
                var schemeName = $(this).attr("value");
                if( schemeName != "" ) {
                    that.loadScheme(schemeName);
                } else {
                    $(".workflowMappingsContainer").addClass("hidden");
                }
            });
        },

        loadScheme: function(schemeName){
            var that = this;
            AJS.log("Loading scheme "+schemeName);
            $.ajax({
                url: contextPath + "/rest/github-importer-plugin/1.0/workflow/scheme?name=" + encodeURIComponent(schemeName),
                type: "get"
            }).done( function(data) {
                    $("#mappings").empty();
                    $.each(data, function(){
                        that.appendWorkflow(this);
                    });
                    $(".workflowMappingsContainer").removeClass("hidden");
                }).fail(function (jqXHR, textStatus, errorThrown){
                    AJS.log(errorThrown);
                });

        },

        appendWorkflow: function(schemeWorkflow) {
            var that = this;

            // workflow Info column
            var workflowInfoTd = $('<td class="padded-column workflow" rowspan="2"/>');
            workflowInfoTd.append( $("<span>").text(schemeWorkflow.workflowName) );
            if( schemeWorkflow.workflowDescription != "" ) {
                var desc = $('<span class="description">').text(schemeWorkflow.workflowDescription);
                workflowInfoTd.append(desc);
            }
            var types = $('<div class="issueTypes" />');
            $.each(schemeWorkflow.associatedTypes, function(){
                var typeSpan = $("<span>")
                    .append($("<img>").attr("src",contextPath+this.iconUrl))
                    .append(document.createTextNode(this.name));
                types.append(typeSpan);
            });
            workflowInfoTd.append(types);

            // select options
            var buildSelectOptions = function(githubIssueState) {
                var options = [];
                $.each(schemeWorkflow.statuses, function(){
                    var option = $("<option>").attr("value",this.id).text(this.name);

                    var statusMapping = that.statusMapping[schemeWorkflow.workflowName];
                    if( statusMapping != null && statusMapping[githubIssueState+"Status"] == this.id ) {
                        // restore what the user had previously selected
                        option.attr("selected", "selected");
                    } else if( this.name.toLowerCase() == githubIssueState ) {
                        // auto-select if similar (Closed -> Closed)
                        option.attr("selected", "selected");
                    }

                    options.push(option);
                });
                return options;
            };

            // select elements
            // jQuery attr is XSS safe. workflowNames are limited to ASCII so there should be problem with using it in the name attribute
            var openSelect = $('<select class="select">').attr("name", schemeWorkflow.workflowName+"_open_status");
            var closedSelect = $('<select class="select">').attr("name", schemeWorkflow.workflowName+"_closed_status");
            $.each( buildSelectOptions("open"), function(){openSelect.append(this)} );
            $.each( buildSelectOptions("closed"), function(){closedSelect.append(this)} );

            // rows
            var tr1 = $("<tr/>")
                .append(workflowInfoTd)
                .append('<td>Open</td>')
                .append('<td class="rightarrowcolumn">&rarr;</td>')
                .append($("<td/>").append(openSelect));

            var tr2 = $("<tr/>")
                .append('<td>Closed</td>')
                .append('<td class="rightarrowcolumn">&rarr;</td>')
                .append($("<td/>").append(closedSelect));

            $("#mappings").append(tr1).append(tr2);
        }
    };

})(AJS.$);