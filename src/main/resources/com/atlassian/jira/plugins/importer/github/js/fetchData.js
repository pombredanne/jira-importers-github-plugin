(function($) {
    "use strict";

    AJS.GithubImporter = AJS.GithubImporter != null ? AJS.GithubImporter : {};

    AJS.GithubImporter.FetchData = {
        externalSystem: null,
        pollDelay: 500,

        fetchNotify: function(fetchProgress){
            var getProgress = function(currentIssue, totalIssues){
                var percentage;
                if( totalIssues != null ) {
                    percentage = (currentIssue / totalIssues) * 100;
                } else {
                    percentage = (currentIssue / 10000) * 100;
                }
                percentage = Math.round(percentage);
                var message = currentIssue + "/" + (totalIssues != null ? totalIssues : "?");
                return {percentage: percentage, message: message};
            };

            var progress = getProgress(fetchProgress.currentClosedIssue, fetchProgress.totalClosedIssues);
            $("#closedIssuesProgressbar").progressBar(progress.percentage);
            $("#closedStatus").text(progress.message);

            progress = getProgress(fetchProgress.currentOpenIssue, fetchProgress.totalOpenIssues);
            $("#openIssuesProgressbar").progressBar(progress.percentage);
            $("#openStatus").text(progress.message);

            if( fetchProgress.currentClosedIssue==fetchProgress.totalClosedIssues && fetchProgress.currentOpenIssue==fetchProgress.totalOpenIssues ) {
                document.location.href = "LabelMappingPage!default.jspa?externalSystem="+this.externalSystem;
            } else {
                // gradually increase the polling delay
                if( this.pollDelay < 3000 ) {
                    this.pollDelay *= 1.2;
                }
                window.setTimeout($.proxy(this.pollProgress,this), this.pollDelay);
            }
        },

        updateMessage: function(type, text) {
            if( text == null ) {
                $("#"+type+"Box").addClass("hidden");
            } else {
                $("#"+type+"Message").text(text);
                $("#"+type+"Box").removeClass("hidden");
            }
        },

        pollProgress: function(){
            var that = this;

            AJS.log("Polling progress");
            $.ajax({
                url: contextPath + "/rest/github-importer-plugin/1.0/fetchData/"+that.externalSystem+"/progress",
                type: "GET",
                cache: false
            }).done( function(data) {
                that.updateMessage("error", data.error);
                that.updateMessage("warning", data.warning);
                that.fetchNotify(data);
            }).fail(function (jqXHR, textStatus, errorThrown){
                AJS.log(errorThrown);
                that.updateMessage("error", "Unable to check fetch progress ("+errorThrown+")");
                window.setTimeout($.proxy(that.pollProgress,that), 2000);
            });
        },

        cancel: function(){
            var that = this;

            AJS.log("Canceling fetch");
            $.ajax({
                url: contextPath + "/rest/github-importer-plugin/1.0/fetchData/"+that.externalSystem+"/cancel",
                type: "POST",
                cache: false
            }).done( function(data) {
                document.location.href = "ImporterProjectMappingsPage!default.jspa?externalSystem="+that.externalSystem;
            }).fail(function (jqXHR, textStatus, errorThrown){
                AJS.log(errorThrown);
                that.updateMessage("error", "Unable to cancel fetch ("+errorThrown+")");
            });

            $("#cancelBtn").attr('disabled','disabled');
        }
    };

})(AJS.$);