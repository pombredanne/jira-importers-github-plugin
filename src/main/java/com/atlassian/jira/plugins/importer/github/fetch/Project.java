package com.atlassian.jira.plugins.importer.github.fetch;

import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Milestone;
import org.eclipse.egit.github.core.Repository;

import java.util.List;

public class Project {
    private Repository repository;
    private List<Issue> issues;
    private List<Milestone> milestones;

    public Project(Repository repository, List<Issue> issues, List<Milestone> milestones) {
        this.repository = repository;
        this.issues = issues;
        this.milestones = milestones;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    public List<Milestone> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<Milestone> milestones) {
        this.milestones = milestones;
    }
}