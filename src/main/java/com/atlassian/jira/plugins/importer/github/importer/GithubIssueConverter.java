package com.atlassian.jira.plugins.importer.github.importer;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.plugins.importer.external.beans.ExternalAttachment;
import com.atlassian.jira.plugins.importer.external.beans.ExternalComment;
import com.atlassian.jira.plugins.importer.external.beans.ExternalIssue;
import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.config.LabelMapping;
import com.atlassian.jira.plugins.importer.github.config.SchemeStatusMapping;
import com.atlassian.jira.plugins.importer.github.fetch.GithubConstants;
import com.atlassian.jira.plugins.importer.github.fetch.Project;
import com.atlassian.jira.plugins.importer.github.rest.workflow.WorkflowService;
import com.atlassian.jira.plugins.importer.imports.importer.ImportLogger;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.eclipse.egit.github.core.Comment;
import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Label;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class GithubIssueConverter {

    private final ConfigBean configBean;
    private final ConstantsManager constantsManager;
    private final WorkflowService workflowService;
    private final AttachmentManager attachmentManager = new AttachmentManager();

    public GithubIssueConverter(ConfigBean configBean, ConstantsManager constantsManager, WorkflowManager workflowManager, WorkflowSchemeManager workflowSchemeManager) {
        this.configBean = configBean;
        this.constantsManager = constantsManager;
        workflowService = new WorkflowService(workflowManager, workflowSchemeManager, constantsManager);
    }

    public ExternalIssue convertIssue(final Project project, final Issue issue, final ImportLogger importLogger) {
        final ExternalIssue externalIssue = new ExternalIssue();
        externalIssue.setExternalId(String.valueOf(issue.getNumber()));
        externalIssue.setReporter(issue.getUser().getLogin());
        if( issue.getAssignee() != null ) {
            externalIssue.setAssignee(issue.getAssignee().getLogin());
        }
        externalIssue.setSummary(issue.getTitle());
        externalIssue.setCreated(issue.getCreatedAt());
        externalIssue.setUpdated(issue.getUpdatedAt());

        String description = issue.getBody();
        if( description != null ) {
            description = attachmentManager.convertMarkdownLinks(description, externalIssue, importLogger);
        }
        externalIssue.setDescription(description);

        IssueType issueType = applyLabels(project, issue, importLogger, externalIssue);

        // apply status (after type is known)
        JiraWorkflow workflow = workflowService.getWorkflow(configBean.getSchemeStatusMapping().getWorkflowSchemeName(), issueType.getId());
        SchemeStatusMapping.JiraStatusMapping jiraStatusMapping = configBean.getSchemeStatusMapping().getWorkflowIdToStatusMapping().get(workflow.getName());

        if( issue.getState().equals(GithubConstants.GITHUB_ISSUE_STATE_OPEN) ) {
            externalIssue.setStatus( jiraStatusMapping.getOpenStatus() );
        } else if( issue.getState().equals(GithubConstants.GITHUB_ISSUE_STATE_CLOSED) ) {
            externalIssue.setStatus( jiraStatusMapping.getClosedStatus() );
        } else {
            throw new IllegalStateException("Unknown GitHub issue state: "+issue.getState());
        }

        // comments
        List<Comment> comments = configBean.getGithubDataService().getComments(issue);
        externalIssue.setComments(Lists.newArrayList(Iterables.transform(comments, new Function<Comment, ExternalComment>() {
            public ExternalComment apply(@Nullable Comment comment) {
                String body = comment.getBody();
                if( body != null ) {
                    body = attachmentManager.convertMarkdownLinks(body, externalIssue, importLogger);
                }
                return new ExternalComment(body, comment.getUser().getLogin(), comment.getCreatedAt());
            }
        })));

        // version
        if( issue.getMilestone() != null ) {
            externalIssue.setFixedVersions(Lists.newArrayList(issue.getMilestone().getTitle()));
        }

        return externalIssue;
    }

    /**
     * Apply issue type, resolution and labels. For convenience returns the complete {@link IssueType}.
     */
    private IssueType applyLabels(Project project, Issue issue, ImportLogger importLogger, ExternalIssue externalIssue) {
        List<LabelMapping> issueTypeLabelMappings = new ArrayList<LabelMapping>();
        List<LabelMapping> issueResolutionLabelMappings = new ArrayList<LabelMapping>();
        List<LabelMapping> remainingLabels = new ArrayList<LabelMapping>();
        IssueType issueType = null;

        for( Label label : issue.getLabels() ) {
            LabelMapping labelMapping = configBean.getMappingsForLabel(label.getName());
            if( labelMapping == null ) {
                throw new IllegalArgumentException("No mappings for label: "+label);
            }

            if( labelMapping.getJiraIssueType() != null || labelMapping.getJiraIssueResolution() != null ) {
                if( labelMapping.getJiraIssueType() != null ) {
                    issueTypeLabelMappings.add(labelMapping);
                }
                if( labelMapping.getJiraIssueResolution() != null ) {
                    issueResolutionLabelMappings.add(labelMapping);
                }
            } else {
                remainingLabels.add(labelMapping);
            }
        }

        if( issueTypeLabelMappings.size() >= 1 ) {
            // check if labels map to different values
            boolean consistent = true;
            for( int i=0; i<issueTypeLabelMappings.size()-1; i++ ) {
                if( !issueTypeLabelMappings.get(i).getJiraIssueType().equals(issueTypeLabelMappings.get(i+1).getJiraIssueType()) ) {
                    consistent = false;
                    break;
                }
            }

            if( consistent ) {
                LabelMapping labelMapping = issueTypeLabelMappings.get(0);
                String id = String.valueOf(labelMapping.getJiraIssueType());
                issueType = constantsManager.getIssueTypeObject(id);
                if( issueType == null ) {
                    throw new RuntimeException("Could not resolve issue type "+id);
                }
            } else {
                importLogger.warn("Conflicting labels for GitHub issue " + issue.getNumber()
                        + ". Labels ("+labelMappingsToString(issueTypeLabelMappings)+") map to different JIRA status values. "
                        + "Using default value.");
                remainingLabels.addAll(issueTypeLabelMappings);
            }
        }

        // apply issueType
        if(issueType == null) {
            // TODO allow the user to select a default type
            issueType = constantsManager.getIssueTypeObject(String.valueOf(IssueFieldConstants.BUG_TYPE_ID));
        }
        externalIssue.setIssueType(issueType.getName());

        // TODO do it without duplicating code
        if( issueResolutionLabelMappings.size() >= 1 ) {
            // check if labels map to different values
            boolean consistent = true;
            for( int i=0; i<issueResolutionLabelMappings.size()-1; i++ ) {
                if( !issueResolutionLabelMappings.get(i).getJiraIssueResolution().equals(issueResolutionLabelMappings.get(i+1).getJiraIssueResolution()) ) {
                    consistent = false;
                    break;
                }
            }

            if( consistent ) {
                LabelMapping labelMapping = issueResolutionLabelMappings.get(0);
                String id = String.valueOf(labelMapping.getJiraIssueResolution());
                Resolution resolution = constantsManager.getResolutionObject(id);
                if( resolution == null ) {
                    throw new RuntimeException("Could not resolve issue resolution "+id);
                }
                externalIssue.setResolution(resolution.getName());
            } else {
                importLogger.warn("Conflicting labels for GitHub issue " + issue.getNumber()
                        + ". Labels (" + labelMappingsToString(issueResolutionLabelMappings) + ") map to different JIRA resolution values. "
                        + "Using default value.");
                remainingLabels.addAll(issueResolutionLabelMappings);
            }
        }

        // labels
        if( configBean.isAutoLabels() ) {
            externalIssue.setLabels( Lists.transform(remainingLabels, new Function<LabelMapping, String>() {
                public String apply(@Nullable LabelMapping labelMapping) {
                    return labelMapping.getLabel();
                }
            }));
        }

        return issueType;
    }

    private String labelMappingsToString(List<LabelMapping> labelMappings) {
        StringBuilder sb = new StringBuilder();
        for( int i=0; i<labelMappings.size(); i++ ) {
            LabelMapping labelMapping = labelMappings.get(i);
            if( i != 0 ) {
                sb.append(", ");
            }
            sb.append(labelMapping.getLabel());
        }
        return sb.toString();
    }

    public List<ExternalAttachment> downloadAttachmentsForIssue(ExternalIssue externalIssue, ImportLogger importLogger) {
        return attachmentManager.downloadAttachmentsForIssue(externalIssue, importLogger);
    }

}
