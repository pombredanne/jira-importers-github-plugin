package com.atlassian.jira.plugins.importer.github.util;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugins.importer.github.fetch.RemoteCall;
import com.atlassian.jira.util.I18nHelper;
import org.apache.log4j.Logger;
import org.eclipse.egit.github.core.client.NoSuchPageException;
import org.eclipse.egit.github.core.client.RequestException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class GithubRequestService {

    private final Logger log = Logger.getLogger(this.getClass());

    private final I18nHelper i18n;
    private final FailureCallback failureCallback;
    private final int maxAttempts;

    /** How often do we give feedback while waiting for the next retry (interval in milli seconds). Null means only at the start and then never. */
    private final Long retryFeedbackInterval;

    public GithubRequestService(FailureCallback failureCallback) {
        this(failureCallback, 100, 1000L);
    }

    public GithubRequestService(FailureCallback failureCallback, int maxAttempts, Long retryFeedbackInterval) {
        i18n = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
        this.failureCallback = failureCallback;
        this.maxAttempts = maxAttempts;
        this.retryFeedbackInterval = retryFeedbackInterval;
    }

    /**
     * Executes the given remote call and repeats on IOExceptions.
     * Can be interrupted.
     */
    public <T> T submit(RemoteCall<T> callable) throws IOException, InterruptedException {
        int attempt = 0;
        while(true) {
            attempt++;

            try {
                if( Thread.currentThread().isInterrupted() ) {
                    throw new InterruptedException();
                }

                T obj = callable.call();
                if( attempt > 1 ) {
                    failureCallback.retriesFinished();
                }
                return obj;

            } catch (RequestException e) {
                // special handling for egit-github exceptions
                // RequestException are thrown if status code was != 200
                if( isRateLimitError(e) ) {
                    long delayMillis = TimeUnit.MINUTES.toMillis(10);
                    sleep(delayMillis, i18n.getText("com.atlassian.jira.plugins.importer.github.fetchData.warning.rateLimit"));
                } else {
                    throw e;
                }

            } catch (IOException e) {
                handleNetworkingException(e, attempt);

            } catch (NoSuchPageException e) {
                // special handling for egit-github exceptions
                if( e.getCause() instanceof IOException ) {
                    IOException wrapper = new IOException("Wrapper for a NoSuchPageException which is actually an IOException", e);
                    handleNetworkingException(wrapper, attempt);
                } else {
                    throw new RuntimeException("Unexpected NoSuchPageException", e);
                }
            }
        }
    }

    private boolean isRateLimitError(RequestException e) {
        // TODO safer with getRemainingRequests
        return e.getStatus() == 403 && e.getError().getMessage().toLowerCase().contains("rate limit");
    }

    private void handleNetworkingException(IOException e, int attempt) throws IOException, InterruptedException {
        if( attempt < maxAttempts ) {
            log.warn("A request to GitHub failed. Will try again", e);

            long delayMillis;
            if( attempt == 1 ) {
                delayMillis = TimeUnit.SECONDS.toMillis(5);
            } else if( attempt == 2 ) {
                delayMillis = TimeUnit.SECONDS.toMillis(30);
            } else {
                delayMillis = TimeUnit.SECONDS.toMillis(120);
            }

            sleep(delayMillis, i18n.getText("com.atlassian.jira.plugins.importer.github.fetchData.warning.network", attempt));
        } else {
            throw new RuntimeException("A request to GitHub failed "+attempt+" times. Giving up", e);
        }
    }

    private void sleep(long delayMillis, String reason) throws InterruptedException {
        long retryTime = System.currentTimeMillis() + delayMillis;
        while( retryTime > System.currentTimeMillis() ) {
            long remainingDelay = retryTime - System.currentTimeMillis();
            long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingDelay);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(remainingDelay) - TimeUnit.MINUTES.toSeconds(minutes);
            String msg = reason + " " + i18n.getText("com.atlassian.jira.plugins.importer.github.fetchData.warning.retry", String.valueOf(minutes), String.valueOf(seconds));
            failureCallback.retry(msg);

            if( retryFeedbackInterval != null ) {
                Thread.sleep(retryFeedbackInterval);
            } else {
                Thread.sleep(delayMillis);
            }
        }
    }

    public interface FailureCallback {
        /** Might be called very often during retry. At the end of retries will be invoked with a null argument. */
        void retry(String msg);
        /** Called if the remote call was successful after a retry */
        void retriesFinished();
    }

}
