package com.atlassian.jira.plugins.importer.github.config;

import com.atlassian.jira.plugins.importer.external.beans.ExternalCustomField;
import com.atlassian.jira.plugins.importer.github.fetch.GithubDataService;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingDefinition;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingDefinitionsFactory;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingHelper;
import com.atlassian.jira.plugins.importer.imports.config.ValueMappingHelperImpl;
import com.atlassian.jira.plugins.importer.imports.importer.AbstractConfigBean2;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigBean extends AbstractConfigBean2 {

    private final GithubDataService githubDataService;
    private List<LabelMapping> labelMappings = new ArrayList<LabelMapping>();
    private boolean autoLabels = true;
    private SchemeStatusMapping schemeStatusMapping;

    public ConfigBean(GithubDataService githubDataService) {
        this.githubDataService = githubDataService;
    }

    @Override
    public List<String> getExternalProjectNames() {
        try {
            return Lists.newArrayList(githubDataService.getAllProjectNames());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getSelectedProjects() {
        List<String> selected = new ArrayList<String>();
        for( String projectName : getExternalProjectNames()) {
            if( isProjectSelected(projectName)) {
                selected.add(projectName);
            }
        }
        return selected;
    }

    @Override
    public List<ExternalCustomField> getCustomFields() {
        return Lists.newArrayList();
    }

    @Override
    public List<String> getLinkNamesFromDb() {
        return Lists.newArrayList();
    }

    @Override
    public ValueMappingHelper initializeValueMappingHelper() {
        // this would be better - but not possible if configuration needs to be saved
        // throw new UnsupportedOperationException("not supported");

        // dummy ValueMappingHelper
        final ValueMappingDefinitionsFactory mappingDefinitionFactory = new ValueMappingDefinitionsFactory() {
            public List<ValueMappingDefinition> createMappingDefinitions(ValueMappingHelper valueMappingHelper) {
                return Lists.newArrayList();

            }
        };
        return new ValueMappingHelperImpl(getWorkflowSchemeManager(), getWorkflowManager(), mappingDefinitionFactory, getConstantsManager());
    }

    public GithubDataService getGithubDataService() {
        return githubDataService;
    }

    public LabelMapping getMappingsForLabel(String label) {
        for( LabelMapping labelMapping : getLabelMappings() ) {
            if( labelMapping.getLabel().equals(label) ) {
                return labelMapping;
            }
        }
        return null;
    }

    public List<LabelMapping> getLabelMappings() {
        return labelMappings;
    }

    public void setLabelMappings(List<LabelMapping> labelMappings) {
        this.labelMappings = labelMappings;
    }

    public boolean isAutoLabels() {
        return autoLabels;
    }

    public void setAutoLabels(boolean autoLabels) {
        this.autoLabels = autoLabels;
    }

    public SchemeStatusMapping getSchemeStatusMapping() {
        return schemeStatusMapping;
    }

    public void setSchemeStatusMapping(SchemeStatusMapping schemeStatusMapping) {
        this.schemeStatusMapping = schemeStatusMapping;
    }
}
