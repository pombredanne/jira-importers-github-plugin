package com.atlassian.jira.plugins.importer.github.rest.workflow;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Convenience methods to access workflow data. Works for JIRA 5 and 6 and provides a single point to upgrade to the new JIRA 6 API on day.
 */
public class WorkflowService {

    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final ConstantsManager constantsManager;

    public WorkflowService(WorkflowManager workflowManager, WorkflowSchemeManager workflowSchemeManager, ConstantsManager constantsManager) {
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.constantsManager = constantsManager;
    }

    /**
     * @param schemeName null stands for the default scheme
     */
    public JiraWorkflow getWorkflow(String schemeName, String issueTypeId) {
        try {
            GenericValue scheme = workflowSchemeManager.getScheme(schemeName);
            return workflowManager.getWorkflowFromScheme(scheme, issueTypeId);
        } catch (GenericEntityException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the workflows of this scheme INCLUDING the jira default workflow if present.
     */
    public Collection<JiraWorkflow> getSchemeWorkflows(String schemeName) {
        List<JiraWorkflow> workflows = new ArrayList<JiraWorkflow>();
        for( IssueType issueType : constantsManager.getAllIssueTypeObjects() ) {
            workflows.add(getWorkflow(schemeName, issueType.getId()));
        }
        return workflows;
    }

    public String getDefaultSchemeName() {
        //return workflowSchemeManager.getDefaultSchemeObject().getName();
        return "Default Workflow Scheme";
    }

    /**
     * Returns all scheme names except the default scheme.
     */
    public List<String> getSchemeNames() {
        return Lists.transform(workflowSchemeManager.getSchemeObjects(), new Function<Scheme, String>() {
            @Override
            public String apply(@Nullable Scheme input) {
                return input.getName();
            }
        });
    }

}
