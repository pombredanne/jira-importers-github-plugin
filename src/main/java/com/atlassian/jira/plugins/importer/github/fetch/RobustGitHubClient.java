package com.atlassian.jira.plugins.importer.github.fetch;

import org.eclipse.egit.github.core.client.GitHubClient;

import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

/**
 * A {@link GitHubClient} that uses connection timeouts.
 */
public class RobustGitHubClient extends GitHubClient {

    @Override
    protected HttpURLConnection configureRequest(HttpURLConnection request) {
        super.configureRequest(request);
        request.setReadTimeout( (int) TimeUnit.SECONDS.toMillis(15) );
        request.setConnectTimeout( (int) TimeUnit.SECONDS.toMillis(15) );
        return request;
    }

}
