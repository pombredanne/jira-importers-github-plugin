package com.atlassian.jira.plugins.importer.github;

import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.fetch.DataFetchJob;
import com.atlassian.jira.plugins.importer.github.fetch.GithubDataService;
import com.atlassian.jira.plugins.importer.web.ImportProcessBean;

/**
 * Keeps the data related to the current import process (in the HTTP session).
 */
public class GithubImportProcessBean extends ImportProcessBean {

    private GithubDataService githubDataService;
    private DataFetchJob dataFetchJob;

    public GithubImportProcessBean(GithubDataService githubDataService, DataFetchJob dataFetchJob, ConfigBean configBean) {
        this.githubDataService = githubDataService;
        this.dataFetchJob = dataFetchJob;
        setConfigBean(configBean);
    }

    /**
     * Makes collected data garbage collectable and stops any running fetch.
     */
    public void cleanup() {
        githubDataService = null;
        dataFetchJob.cancel();
        dataFetchJob = null;
    }

    public GithubDataService getGithubDataService() {
        return githubDataService;
    }

    public DataFetchJob getDataFetchJob() {
        return dataFetchJob;
    }
}
