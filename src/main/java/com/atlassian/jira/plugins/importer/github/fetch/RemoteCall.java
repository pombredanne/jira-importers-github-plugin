package com.atlassian.jira.plugins.importer.github.fetch;

import java.io.IOException;

/**
 * A remote call that might fail with an IOException.
 */
public interface RemoteCall<V> {

    V call() throws IOException;

}
