package com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.config.LabelMapping;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import org.eclipse.egit.github.core.Label;
import webwork.action.ActionContext;

import java.util.*;

/**
 * Enables the user to map GitHub issue labels to JIRA issue types and issue resolutions.
 */
public class LabelMappingPage extends AbstractSetupPage {

    public LabelMappingPage(UsageTrackingService usageTrackingService,
                                     WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if( isNextClicked() ) {
            getConfigBean().setAutoLabels( ActionContext.getParameters().containsKey("autoLabels") );
            populateLabelMappings();
        }
        if( isPreviousClicked() ) {
            return getRedirect("ImporterProjectMappingsPage!default.jspa?externalSystem=" + getExternalSystem());
        }

        return super.doExecute();
    }

    private void populateLabelMappings() {
        Map<String,String> actionParams = ActionContext.getParameters();

        List<LabelMapping> labelMappings = new ArrayList<LabelMapping>();
        for( Label label : getConfigBean().getGithubDataService().getLabels() ) {
            LabelMapping labelMapping = new LabelMapping();
            labelMappings.add(labelMapping);
            labelMapping.setLabel(label.getName());

            String issueTypeStr = ParameterUtils.getStringParam(actionParams, "typeMapping_"+label);
            if( !issueTypeStr.isEmpty() ) {
                labelMapping.setJiraIssueType(Integer.valueOf(issueTypeStr));
            }

            String issueResolutionStr = ParameterUtils.getStringParam(actionParams, "resolutionMapping_"+label);
            if( !issueResolutionStr.isEmpty() ) {
                labelMapping.setJiraIssueResolution(Integer.valueOf(issueResolutionStr));
            }
        }
        getConfigBean().setLabelMappings(labelMappings);
    }

    @Override
    public String getFormTitle() {
        return getText("com.atlassian.jira.plugins.importer.github.labelmapping.title");
    }

    public ConfigBean getConfigBean() {
        try {
            if (getController() == null || getController().getImportProcessBeanFromSession() == null) {
                throw new IllegalStateException("Importer not intialized");
            }
            ConfigBean configBean = (ConfigBean) getController().getImportProcessBeanFromSession().getConfigBean();
            return configBean;
        } catch (ClassCastException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, String> getJiraIssueTypeValues() {
        ConstantsManager constantsManager = ComponentAccessor.getConstantsManager();
        Map<String, String> options = new LinkedHashMap<String, String>();
        for( IssueType issueType : constantsManager.getAllIssueTypeObjects() ) {
            options.put(issueType.getId(), issueType.getNameTranslation());
        }
        return options;
    }

    public Map<String, String> getJiraIssueResolutionValues() {
        ConstantsManager constantsManager = ComponentAccessor.getConstantsManager();
        Map<String, String> options = new LinkedHashMap<String, String>();
        for( Resolution resolution : constantsManager.getResolutionObjects() ) {
            options.put(resolution.getId(), resolution.getNameTranslation());
        }
        return options;
    }

    public String getSelectedIssueTypeMapping(String label) {
        LabelMapping labelMapping = getConfigBean().getMappingsForLabel(label);
        if( labelMapping != null && labelMapping.getJiraIssueType() != null ) {
            return String.valueOf(labelMapping.getJiraIssueType());
        } else {
            return "";
        }
    }

    public String getSelectedIssueResolutionMapping(String label) {
        LabelMapping labelMapping = getConfigBean().getMappingsForLabel(label);
        if( labelMapping != null && labelMapping.getJiraIssueResolution() != null ) {
            return String.valueOf(labelMapping.getJiraIssueResolution());
        } else {
            return "";
        }
    }

}
