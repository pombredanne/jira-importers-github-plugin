package com.atlassian.jira.plugins.importer.github.fetch;

import com.google.gson.reflect.TypeToken;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.client.PageIterator;
import org.eclipse.egit.github.core.client.PagedRequest;
import org.eclipse.egit.github.core.service.GitHubService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.eclipse.egit.github.core.client.IGitHubConstants.SEGMENT_STARRED;
import static org.eclipse.egit.github.core.client.IGitHubConstants.SEGMENT_USER;
import static org.eclipse.egit.github.core.client.PagedRequest.PAGE_FIRST;
import static org.eclipse.egit.github.core.client.PagedRequest.PAGE_SIZE;

/**
 * Extension of egit github to load starred repositories.
 */
public class StarredRepositoryService extends GitHubService {

    public StarredRepositoryService(GitHubClient client) {
        super(client);
    }

    /**
     * Get starred repositories for the currently authenticated user
     *
     * @return list of repositories
     * @throws java.io.IOException
     */
    public List<Repository> getStarredRepositories() throws IOException {
        return getAll(pageStarredRepositories());
    }

    /**
     * Page starred repositories for currently authenticated user
     *
     * @return iterator over pages of repositories
     */
    public PageIterator<Repository> pageStarredRepositories() {
        return pageStarredRepositories((Map<String, String>) null, PAGE_FIRST, PAGE_SIZE);
    }

    /**
     * Page starred repositories for currently authenticated user
     *
     * @param filterData
     * @param start
     * @param size
     * @return iterator over pages of repositories
     */
    public PageIterator<Repository> pageStarredRepositories(
            Map<String, String> filterData, int start, int size) {
        PagedRequest<Repository> request = createPagedRequest(start, size);
        request.setUri(SEGMENT_USER + SEGMENT_STARRED);
        request.setParams(filterData);
        request.setType(new TypeToken<List<Repository>>() {
        }.getType());
        return createPageIterator(request);
    }

}