package com.atlassian.jira.plugins.importer.github.fetch;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.I18nHelper;
import org.apache.log4j.Logger;
import org.eclipse.egit.github.core.client.RequestException;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A job that loads the data of all selected projects by calling {@link GithubDataService#loadProject} and records the progress.
 * Can be cancelled.
 */
@ThreadSafe
public class DataFetchJob {

    private final Logger log = Logger.getLogger(this.getClass());

    private final GithubDataService githubDataService;
    private final AtomicReference<FutureTask<Void>> futureTaskAtomicReference = new AtomicReference<FutureTask<Void>>();
    private final I18nHelper i18n;

    @GuardedBy("this")
    private FetchProgress _fetchProgress = new FetchProgress();

    public DataFetchJob(GithubDataService githubDataService) {
        this.githubDataService = githubDataService;
        i18n = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
    }

    /**
     * Perform the fetch for the given projects.
     * Does not throw an exception.
     */
    public void run(List<String> selectedProjects) {
        FutureTask<Void> future = new FutureTask<Void>(new DataFetchCallable(selectedProjects));
        if( futureTaskAtomicReference.compareAndSet(null, future) ) {
            try {
                log.info("Starting data fetch job");
                future.run();
            } finally {
                log.info("Data fetch job has stopped");
                futureTaskAtomicReference.set(null);
            }
        } else {
            throw new IllegalStateException("Fetch job is already running");
        }
    }

    /**
     * Cancels the fetch or does nothing if no fetch is running.
     */
    public void cancel() {
        FutureTask<Void> future = futureTaskAtomicReference.get();
        if( future != null ) {
            log.info("Cancelling data fetch job");
            future.cancel(true);
        }
    }

    public boolean isRunning() {
        return futureTaskAtomicReference.get() != null;
    }

    private class DataFetchCallable implements Callable<Void> {
        private final List<String> selectedProjects;

        private DataFetchCallable(List<String> selectedProjects) {
            this.selectedProjects = selectedProjects;
        }

        @Override
        public Void call() throws Exception {
            try {
                githubDataService.clearLoadedProjectData();
                setFetchProgress(new FetchProgress());

                // fetch all
                for(String projectName : selectedProjects ) {
                    githubDataService.loadProject(projectName, new GithubDataService.ProgressFeedback(){
                        @Override
                        public void onProgress(FetchProgress fetchProgress) {
                            setFetchProgress(fetchProgress);
                        }
                    });
                }

            } catch (InterruptedException e) {
                githubDataService.clearLoadedProjectData();
                throw e;

            } catch (Exception e) {
                if( e instanceof RequestException && ((RequestException)e).getStatus() == 410 ) {
                    // issues are disabled in GitHub
                    FetchProgress fetchProgress = new FetchProgress();
                    fetchProgress.setError(i18n.getText("com.atlassian.jira.plugins.importer.github.fetchData.error.issuesDisabled"));
                    setFetchProgress(fetchProgress);
                    log.warn("Fetching data from GitHub failed", e);
                } else {
                    FetchProgress fetchProgress = new FetchProgress();
                    fetchProgress.setError(i18n.getText("com.atlassian.jira.plugins.importer.github.fetchData.error.otherExceptions"));
                    setFetchProgress(fetchProgress);
                    log.error("Fetching data from GitHub failed", e);
                }
            }
            return null;
        }
    }

    public FetchProgress getFetchProgress() {
        synchronized (this) {
            return _fetchProgress;
        }
    }

    public void setFetchProgress(FetchProgress fetchProgress) {
        synchronized (this) {
            this._fetchProgress = fetchProgress;
        }
    }

}
