package com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.GithubImportProcessBean;
import com.atlassian.jira.plugins.importer.github.GithubImporterController;
import com.atlassian.jira.plugins.importer.github.fetch.DataFetchJob;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import webwork.action.ActionContext;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Here the majority of data is fetched from GitHub. Gives the user some feedback of what is happening.
 */
public class FetchDataPage extends AbstractSetupPage {

    private static final String SELECTED_PROJECTS_KEY = "SELECTED_PROJECTS_KEY";
    private final TaskManager taskManager;

    public FetchDataPage(UsageTrackingService usageTrackingService, WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor, TaskManager taskManager) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
        this.taskManager = taskManager;
    }

    @Override
    public String doDefault() throws Exception {
        FetchState fetchState = getFetchState();

        if( fetchState == FetchState.NOT_STARTED) {
            startFetchJob();
        } else {
            if( isSelectionUnchanged() ) {
                if( fetchState == FetchState.RUNNING ) {
                    // do nothing - just display the fetch page
                }
                if( fetchState == FetchState.FINISHED) {
                    // makes sure refreshing the fetch page after a finished fetch does not start it again
                    // (in case of websudo confirmation is canceled or front-end error)
                    return getRedirect("LabelMappingPage!default.jspa?externalSystem=" + getExternalSystem());
                }
            } else {
                if( fetchState == FetchState.RUNNING ) {
                    // somehow the user got back to the project selection page and selected another project
                    throw new IllegalStateException("Project selection has changed during a running fetch");
                }
                if( fetchState == FetchState.FINISHED) {
                    // do it again for different projects
                    startFetchJob();
                }
            }
        }

        return INPUT;
    }

    private void startFetchJob() {
        TaskContext taskContext = new TaskContext() {
            public String buildProgressURL(Long aLong) {
                return null;
            }
        };

        final ConfigBean configBean = getConfigBean();
        final DataFetchJob dataFetchJob = getProcessBean().getDataFetchJob();
        Callable<Void> task = new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                dataFetchJob.run(configBean.getSelectedProjects());
                return null;
            }
        };

        TaskDescriptor<Void> taskDescriptor = taskManager.submitTask(task, "GitHub importer fetch data task", taskContext);

        ActionContext.getSession().put(SELECTED_PROJECTS_KEY, configBean.getSelectedProjects());
    }

    private boolean isSelectionUnchanged() {
        List<String> previouslySelected = (List<String>) ActionContext.getSession().get(SELECTED_PROJECTS_KEY);
        if( previouslySelected != null ) {
            return previouslySelected.containsAll(getConfigBean().getSelectedProjects());
        } else {
            throw new IllegalStateException();
        }
    }

    private FetchState getFetchState() {
        FetchState fetchState;
        if( getProcessBean().getDataFetchJob().isRunning() ) {
            fetchState = FetchState.RUNNING;
        } else if( getProcessBean().getGithubDataService().hasDataBeenLoaded() ) {
            fetchState = FetchState.FINISHED;
        } else {
            fetchState = FetchState.NOT_STARTED;
        }
        return fetchState;
    }

    public String getExternalSystem() {
        return GithubImporterController.EXTERNAL_SYSTEM;
    }

    private ConfigBean getConfigBean() {
        return (ConfigBean) getProcessBean().getConfigBean();
    }

    private GithubImportProcessBean getProcessBean() {
        try {
            if (getController() == null || getController().getImportProcessBeanFromSession() == null) {
                throw new IllegalStateException("Importer not intialized");
            }
            return (GithubImportProcessBean) getController().getImportProcessBeanFromSession();
        } catch (ClassCastException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getFormTitle() {
        return getText("com.atlassian.jira.plugins.importer.github.fetchData.title");
    }

    enum FetchState {NOT_STARTED, RUNNING, FINISHED}

}
