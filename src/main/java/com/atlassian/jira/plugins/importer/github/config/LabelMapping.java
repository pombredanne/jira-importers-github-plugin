package com.atlassian.jira.plugins.importer.github.config;

import java.io.Serializable;

/**
 * The mapping information comes directly from the user or a stored configuration file and might thus have been manipulated or might be invalid.
 */
public class LabelMapping implements Serializable {
    private String label;
    private Integer jiraIssueType;
    private Integer jiraIssueResolution;

    public LabelMapping() {
    }

    public LabelMapping(String label, Integer jiraIssueType, Integer jiraIssueResolution) {
        this.label = label;
        this.jiraIssueType = jiraIssueType;
        this.jiraIssueResolution = jiraIssueResolution;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getJiraIssueType() {
        return jiraIssueType;
    }

    public void setJiraIssueType(Integer jiraIssueType) {
        this.jiraIssueType = jiraIssueType;
    }

    public Integer getJiraIssueResolution() {
        return jiraIssueResolution;
    }

    public void setJiraIssueResolution(Integer jiraIssueResolution) {
        this.jiraIssueResolution = jiraIssueResolution;
    }
}