package com.atlassian.jira.plugins.importer.github.fetch;

import com.atlassian.jira.plugins.importer.github.util.GithubRequestService;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.eclipse.egit.github.core.*;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.client.PageIterator;
import org.eclipse.egit.github.core.service.IssueService;
import org.eclipse.egit.github.core.service.LabelService;
import org.eclipse.egit.github.core.service.MilestoneService;
import org.eclipse.egit.github.core.service.RepositoryService;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;

/**
 * Encapsulates GitHub data access.
 */
public class GithubDataService {

    private final Logger log = Logger.getLogger(this.getClass());

    private String accessToken;

    private List<Repository> repositories;

    private final Map<String, Project> projects = new HashMap<String, Project>();
    private final List<Issue> allIssues = new ArrayList<Issue>();
    private final Set<Label> allLabels = new HashSet<Label>();
    private final Map<Issue, List<Comment>> issueComments = new HashMap<Issue, List<Comment>>();

    private void loadRepositories() throws IOException {
        // TODO missing handling of IOExceptions

        if(repositories == null ) {
            if( accessToken == null ) {
                throw new IllegalStateException("accessToken has not been set");
            }

            log.debug("Loading repositories");
            GitHubClient client = new GitHubClient();
            client.setOAuth2Token(accessToken);
            RepositoryService service = new RepositoryService(client);
            repositories = service.getRepositories();

            StarredRepositoryService starredRepositoryService = new StarredRepositoryService(client);
            List<Repository> starredRepositories = starredRepositoryService.getStarredRepositories();
            repositories.addAll(starredRepositories);
        }
    }

    public List<String> getAllProjectNames() throws IOException {
        loadRepositories();

        return Lists.transform(repositories, new Function<Repository, String>() {
            @Override
            public String apply(@Nullable Repository input) {
                return input.generateId();
            }
        });
    }

    public void clearLoadedProjectData() {
        projects.clear();
        allIssues.clear();
        allLabels.clear();
        issueComments.clear();
    }

    public void loadProject(final String projectName, final ProgressFeedback progressFeedback) throws IOException, InterruptedException {
        if( projects.containsKey(projectName) ) {
            throw new IllegalStateException("Project '"+projectName+"' already loaded");
        }
        log.debug("Loading project "+projectName);
        Repository repository = getRepository(projectName);

        final FetchProgress fetchProgress = new FetchProgress();

        GithubRequestService requestService = new GithubRequestService(new GithubRequestService.FailureCallback() {
            @Override
            public void retry(String msg) {
                fetchProgress.setWarning(msg);
                progressFeedback.onProgress(fetchProgress);
            }

            @Override
            public void retriesFinished() {
                fetchProgress.setWarning(null);
                progressFeedback.onProgress(fetchProgress);
            }
        });

        // load labels and milestones
        loadAndAddRepositoryLabels(repository, requestService);
        List<Milestone> milestones = loadMilestones(repository, requestService);

        // load closed and open issues
        Map<String, String> filterData = new HashMap<String, String>();
        filterData.put(GithubConstants.GITHUB_ISSUE_STATE, GithubConstants.GITHUB_ISSUE_STATE_CLOSED);
        List<Issue> projectIssues = loadProjectIssues(repository, filterData, new InternalProgressFeedback() {
            @Override
            public void onProgress(int currentIssue, Integer totalIssues) {
                fetchProgress.setCurrentClosedIssue(currentIssue);
                fetchProgress.setTotalClosedIssues(totalIssues);
                progressFeedback.onProgress(fetchProgress);
            }
        }, requestService);

        filterData.put(GithubConstants.GITHUB_ISSUE_STATE, GithubConstants.GITHUB_ISSUE_STATE_OPEN);
        projectIssues.addAll(loadProjectIssues(repository, filterData, new InternalProgressFeedback() {
            @Override
            public void onProgress(int currentIssue, Integer totalIssues) {
                fetchProgress.setCurrentOpenIssue(currentIssue);
                fetchProgress.setTotalOpenIssues(totalIssues);
                progressFeedback.onProgress(fetchProgress);
            }
        }, requestService));

        // sort issues by external issue key
        Collections.sort(projectIssues, new Comparator<Issue>() {
            @Override
            public int compare(Issue i1, Issue i2) {
                return i1.getNumber() - i2.getNumber();
            }
        });

        projects.put(projectName, new Project(repository, projectIssues, milestones));
        allIssues.addAll(projectIssues);
    }


    private void loadAndAddRepositoryLabels(final Repository repository, GithubRequestService requestService) throws IOException, InterruptedException {
        GitHubClient client = getGitHubClient();
        final LabelService labelService = new LabelService(client);
        List<Label> issueLabels = requestService.submit(new RemoteCall<List<Label>>() {
            public List<Label> call() throws IOException {
                return labelService.getLabels(repository);
            }
        });
        allLabels.addAll(issueLabels);
    }

    private List<Milestone> loadMilestones(final Repository repository, GithubRequestService requestService) throws IOException, InterruptedException {
        final MilestoneService milestoneService = new MilestoneService(getGitHubClient());

        List<Milestone> closedMilestones = requestService.submit(new RemoteCall<List<Milestone>>() {
            @Override
            public List<Milestone> call() throws IOException {
                return milestoneService.getMilestones(repository, GithubConstants.GITHUB_MILESTONE_STATE_CLOSED);
            }
        });
        List<Milestone> openMilestones = requestService.submit(new RemoteCall<List<Milestone>>() {
            @Override
            public List<Milestone> call() throws IOException {
                return milestoneService.getMilestones(repository, GithubConstants.GITHUB_MILESTONE_STATE_OPEN);
            }
        });

        return new ImmutableList.Builder<Milestone>()
                .addAll(closedMilestones)
                .addAll(openMilestones)
                .build();
    }

    private List<Issue> loadProjectIssues(final Repository repository, final Map<String, String> filterData, InternalProgressFeedback progress, GithubRequestService requestService) throws IOException, InterruptedException {
        GitHubClient client = getGitHubClient();
        final IssueService issueService = new IssueService(client);

        List<Issue> projectIssues = new ArrayList<Issue>();
        int currentIssue = 0;
        Integer totalIssues= null;

        final PageIterator<Issue> pager = requestService.submit(new RemoteCall<PageIterator<Issue>>() {
            @Override
            public PageIterator<Issue> call() throws IOException {
                return issueService.pageIssues(repository, filterData);
            }
        });

        while (pager.hasNext()) {
            Collection<Issue> pageIssues = requestService.submit(new RemoteCall<Collection<Issue>>() {
                @Override
                public Collection<Issue> call() throws IOException {
                    return pager.next();
                }
            });

            projectIssues.addAll(pageIssues);

            if( totalIssues == null ) {
                if( pager.getLastPage() != -1 ) {
                    // there is more than one page - estimate total
                    totalIssues = pager.getLastPage() * pageIssues.size();
                } else {
                    // this is the only page
                    totalIssues = pageIssues.size();
                }
            }

            // completely load data for every issue and send progress feedback
            for( final Issue issue : pageIssues ) {
                // comments
                List<Comment> comments = requestService.submit(new RemoteCall<List<Comment>>() {
                    @Override
                    public List<Comment> call() throws IOException {
                        return issueService.getComments(repository, String.valueOf(issue.getNumber()));
                    }
                });
                issueComments.put(issue, comments);

                currentIssue++;
                progress.onProgress(currentIssue, totalIssues);
            }
        }

        if( totalIssues == null ) {
            // there was not a single result page
            totalIssues = 0;
        } else {
            // totalIssues was just a guess. setting it to the correct value now
            totalIssues = currentIssue;
        }
        progress.onProgress(currentIssue, totalIssues);

        return projectIssues;
    }

    private void ensureDataIsLoaded() {
        if( !hasDataBeenLoaded() ) {
            throw new IllegalStateException("Data has not been loaded yet");
        }
    }

    public boolean hasDataBeenLoaded() {
        return !projects.isEmpty();
    }

    public List<Project> getProjects() {
        ensureDataIsLoaded();
        return Lists.newArrayList(projects.values());
    }

    public Project getProjectByName(String projectName) {
        ensureDataIsLoaded();
        return projects.get(projectName);
    }

    public List<Issue> getAllIssues() {
        ensureDataIsLoaded();
        return allIssues;
    }

    public List<Label> getLabels() {
        ensureDataIsLoaded();
        List<Label> labels = new ArrayList<Label>(allLabels);
        Collections.sort(labels, new Comparator<Label>() {
            public int compare(Label o1, Label o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return labels;
    }

    public List<Comment> getComments(Issue issue) {
        ensureDataIsLoaded();
        return issueComments.get(issue);
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    private Repository getRepository(String id) {
        if(repositories == null ) {
            throw new IllegalStateException("Repositories not loaded yet");
        }

        Repository repository = null;
        for( Repository repo : repositories ) {
            if( repo.generateId().equals(id) ) {
                repository = repo;
                break;
            }
        }
        if( repository == null ) {
            throw new IllegalArgumentException("Repository '"+id+"' not found");
        }
        return repository;
    }

    private GitHubClient getGitHubClient() {
        GitHubClient client = new RobustGitHubClient();
        client.setOAuth2Token(accessToken);
        return client;
    }

    public static interface ProgressFeedback {
        void onProgress(FetchProgress fetchProgress);
    }

    private static interface InternalProgressFeedback {
        void onProgress(int currentIssue, Integer totalIssues);
    }

}
